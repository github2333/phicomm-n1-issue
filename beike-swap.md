贝壳云增加 file swap
=================

说明：
> 这里的`/media/ssd001` 是我的SSD磁盘的挂载点，实际使用请按自己的路径打命令，不可能照抄。


有单独分区的，可以用单独的分区，没有的话直接用文件也行，像这样（分配一个2G的文件用作swap）：

`fallocate -l 2048M /media/ssd001/swapfile`


设置好权限：
`chmod 600 /media/ssd001/swapfile`

创建swap:
```
[root@beikeyun:/media/ssd001]# mkswap /media/ssd001/swapfile
Setting up swapspace version 1, size = 2 GiB (2147479552 bytes)
no label, UUID=9e8b09b4-eeb7-404c-8913-26dd1d20ff42
```

启用swap（这个命令每次开机之后都要执行）:
```
swapon /media/ssd001/swapfile
```


更多信息请参考：
https://wiki.archlinux.org/index.php/Swap#Swap_file

