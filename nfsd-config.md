N1 服务端配置:

建立导出目录:

```shell
mkdir -p /srv/nfs/qb
```

绑定目录到导出目录:
```shell
mount --bind /media/wd3t/qbittorrent/download/ /srv/nfs/qb
```
这一行bind 可以添加到开机脚本rc.local中.可在dashboard中设置.


编辑:
```shell
vim /etc/exports
```

增加两行:
```shell
/srv/nfs        192.168.8.0/24(rw,sync,crossmnt,no_root_squash,no_subtree_check,insecure,fsid=0)
/srv/nfs/qb     192.168.8.0/24(rw,sync,no_root_squash,insecure,no_subtree_check)
```

告诉nfsd 我加了新东西了:
```shell
[root@phicomm-n1:/etc/init.d]#  exportfs -rav
exporting 192.168.8.0/24:/srv/nfs/qb
exporting 192.168.8.0/24:/srv/nfs
```

查看状态:
```shell
[root@phicomm-n1:/etc/init.d]#  exportfs -v
/srv/nfs      	192.168.8.0/24(rw,wdelay,crossmnt,insecure,no_root_squash,no_subtree_check,fsid=0,sec=sys,rw,insecure,no_root_squash,no_all_squash)
/srv/nfs/qb   	192.168.8.0/24(rw,wdelay,insecure,no_root_squash,no_subtree_check,sec=sys,rw,insecure,no_root_squash,no_all_squash)

```

KO

---------------------------------------------------------

Android 挂载:

先开启写:

```
mount -orw,remount /dev/block/system
```

N1 rush 固件配置:
编辑NFS脚本 /system/etc/nfs_mounts.sh
修改最后一行为:
```shell
busybox mount -o ro,noatime,nolock,hard,intr,rsize=32768,wsize=32768,vers=3 -t nfs 192.168.8.108:/srv/nfs/qb /mnt/nfs
```
---------------------------------------------------------

T1 rush 固件配置:

编辑NFS脚本 /system/etc/nfs_mounts.sh

```
SERV='192.168.8.108:/srv/nfs/qb'
DEST='qb'
mountnfs3
```

最终脚本类似:
```shell
q201:/mnt # cat /system/etc/nfs_mounts.sh
SERV=''
DEST=''
function mountnfs3()
{
mkdir /mnt/$DEST
chgrp root /mnt/$DEST
chmod 0644 /mnt/$DEST
busybox mount -o ro,nolock,hard,intr,vers=3 -t nfs $SERV /mnt/$DEST
}

SERV='192.168.8.108:/srv/nfs/qb'
DEST='qb'
mountnfs3

mount --bind /mnt/qb /sdcard/Movies/

exit 0

```